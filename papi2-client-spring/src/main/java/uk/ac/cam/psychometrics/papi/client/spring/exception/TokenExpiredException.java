package uk.ac.cam.psychometrics.papi.client.spring.exception;

public class TokenExpiredException extends PapiClientException {

    public TokenExpiredException(final String message, final Throwable cause) {
        super(message, cause);
    }

}
