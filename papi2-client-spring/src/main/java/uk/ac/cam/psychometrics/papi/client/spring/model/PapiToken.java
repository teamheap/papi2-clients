package uk.ac.cam.psychometrics.papi.client.spring.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Objects;

public class PapiToken implements Serializable {

    private final String tokenString;
    private final Long expires;

    @JsonCreator
    public PapiToken(@JsonProperty("token") final String tokenString,
                     @JsonProperty("expires") final Long expires) {
        this.tokenString = tokenString;
        this.expires = expires;
    }

    public String getTokenString() {
        return tokenString;
    }

    public Long getExpires() {
        return expires;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PapiToken{");
        sb.append("tokenString='").append(tokenString).append('\'');
        sb.append(", expires=").append(expires);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(tokenString);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final PapiToken other = (PapiToken) obj;
        return Objects.equals(this.tokenString, other.tokenString);
    }
}
