package uk.ac.cam.psychometrics.papi.client.spring.exception;

public class TokenInvalidException extends PapiClientException {

    public TokenInvalidException(final String message, final Throwable cause) {
        super(message, cause);
    }

}
