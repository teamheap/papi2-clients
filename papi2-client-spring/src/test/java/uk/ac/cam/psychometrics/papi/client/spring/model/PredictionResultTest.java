package uk.ac.cam.psychometrics.papi.client.spring.model;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class PredictionResultTest {

    private static final String UID_1 = "1";
    private static final String UID_2 = "2";
    private static final Set<Prediction> TRAITS_1 = new HashSet<Prediction>() {{
        add(new Prediction(Trait.BIG5_OPENNESS, 1));
        add(new Prediction(Trait.BIG5_CONSCIENTIOUSNESS, 1));
        add(new Prediction(Trait.BIG5_EXTRAVERSION, 1));
    }};
    private static final Set<Prediction> TRAITS_2 = new HashSet<Prediction>() {{
        add(new Prediction(Trait.BIG5_OPENNESS, 0.5));
        add(new Prediction(Trait.BIG5_CONSCIENTIOUSNESS, 0.5));
        add(new Prediction(Trait.BIG5_EXTRAVERSION, 0.5));
    }};

    @Test
    public void shouldEqualWhenHasSamePrediction() throws Exception {
        PredictionResult predictionResult1 = new PredictionResult(UID_1, TRAITS_1);
        PredictionResult predictionResult2 = new PredictionResult(UID_1, TRAITS_1);
        assertEquals(predictionResult1, predictionResult2);
        assertEquals(predictionResult1.hashCode(), predictionResult2.hashCode());
    }

    @Test
    public void shouldNotEqualWhenHasDifferentPrediction() throws Exception {
        PredictionResult predictionResult1 = new PredictionResult(UID_1, TRAITS_1);
        PredictionResult predictionResult2 = new PredictionResult(UID_1, TRAITS_2);
        assertNotEquals(predictionResult1, predictionResult2);
        assertNotEquals(predictionResult1.hashCode(), predictionResult2.hashCode());
    }

    @Test
    public void shouldNotEqualWhenWasDoneForDifferentUser() throws Exception {
        PredictionResult predictionResult1 = new PredictionResult(UID_1, TRAITS_1);
        PredictionResult predictionResult2 = new PredictionResult(UID_2, TRAITS_1);
        assertNotEquals(predictionResult1, predictionResult2);
        assertNotEquals(predictionResult1.hashCode(), predictionResult2.hashCode());
    }

}
