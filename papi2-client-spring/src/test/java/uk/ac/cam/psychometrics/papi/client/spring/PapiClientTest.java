package uk.ac.cam.psychometrics.papi.client.spring;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.client.RestTemplate;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

@RunWith(MockitoJUnitRunner.class)
public class PapiClientTest {

    private static final String SERVICE_URL = "http://localhost/";

    @Mock
    private RestTemplate restTemplate;

    @Test
    public void shouldReturnResources() throws Exception {
        PapiClient client = new PapiClient(new PapiClient.Config().withHttpEndpoint(restTemplate, SERVICE_URL));
        assertNotNull(client.getAuthResource());
        assertNotNull(client.getPredictionResource());
    }

    @Test
    public void shouldNotReturnAuthResourceIfNotConfigured() throws Exception {
        PapiClient client = new PapiClient(new PapiClient.Config());
        try {
            client.getAuthResource();
            fail("Expected exception");
        } catch (IllegalStateException expected) {
        }
    }

    @Test
    public void shouldNotReturnPredictionResourceIfNotConfigured() throws Exception {
        PapiClient client = new PapiClient(new PapiClient.Config());
        try {
            client.getPredictionResource();
            fail("Expected exception");
        } catch (IllegalStateException expected) {
        }
    }

}