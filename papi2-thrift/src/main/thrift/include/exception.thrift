namespace java uk.ac.cam.psychometrics.papi.thrift.exception
namespace php Papi.Thrift.Exception

exception TNotAuthenticatedException {
    1: string message;
}

exception TTokenExpiredException {
    1: string message;
}

exception TTokenInvalidException {
    1: string message;
}

exception TUsageLimitExceededException {
    1: string message;
}