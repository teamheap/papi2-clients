package uk.ac.cam.psychometrics.papi.client.jersey.exception;

public class NoPredictionException extends Exception {

    public NoPredictionException(final String message) {
        super(message);
    }

}
