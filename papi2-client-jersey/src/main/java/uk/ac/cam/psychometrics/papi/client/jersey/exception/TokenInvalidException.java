package uk.ac.cam.psychometrics.papi.client.jersey.exception;

public class TokenInvalidException extends PapiClientException {

    public TokenInvalidException(final String message) {
        super(message);
    }

}
