package uk.ac.cam.psychometrics.papi.client.jersey.example;

import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.jackson.JacksonFeature;
import uk.ac.cam.psychometrics.papi.client.jersey.PapiClient;
import uk.ac.cam.psychometrics.papi.client.jersey.exception.NoPredictionException;
import uk.ac.cam.psychometrics.papi.client.jersey.model.PapiToken;
import uk.ac.cam.psychometrics.papi.client.jersey.model.PredictionResult;
import uk.ac.cam.psychometrics.papi.client.jersey.model.Trait;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * To compile this example with Maven, add the following artifacts as dependencies:
 * <p/>
 * org.glassfish.jersey.core/jersey-client-2.6
 * org.glassfish.jersey.media/jersey-media-json-jackson-2.6
 * org.glassfish.jersey.connectors/jersey-apache-connector-2.6
 */
public class Example {

    private static final Logger LOGGER = Logger.getLogger("Example");
    private static final String SERVICE_URL = "http://api.applymagicsauce.com";
    private static final int CUSTOMER_ID = 1;
    private static final String API_KEY = "apiKey";
    private static final String UID = "1";
    private static final Set<String> LIKE_IDS = new HashSet<String>() {{
        add("7010901522");
        add("7721750727");
        add("7557552517");
        add("8536905548");
        add("7723400562");
        add("8800570812");
        add("10765693196");
        add("14269799090");
        add("12938634034");
        add("14287253499");
    }};
    private static final Set<Trait> TRAITS = new HashSet<Trait>() {{
        add(Trait.BIG5);
        add(Trait.GAY);
    }};

    public static void main(String[] args) {
        new Example().run();
    }

    void run() {
        // Creates the client. It is using Jersey client configured with
        // JacksonFeature provided by Jackson.
        PapiClient papiClient = new PapiClient(new PapiClient.Config()
                .withHttpEndpoint(getJerseyClient(), SERVICE_URL));

        // Authentication. This token will be valid for at least one hour, so you want to store it
        // and re-use for further requests
        PapiToken papiToken = papiClient.getAuthResource().requestToken(CUSTOMER_ID, API_KEY);

        // Get predictions and print
        try {
            PredictionResult predictionResult = papiClient.getPredictionResource().getByLikeIds(
                    TRAITS, papiToken.getTokenString(), UID, LIKE_IDS);
            LOGGER.info(predictionResult.toString());
        } catch (NoPredictionException e) {
            LOGGER.log(Level.INFO, "No prediction could be made", e);
        }
    }

    private Client getJerseyClient() {
        Client client = ClientBuilder.newClient(getJerseyClientConfig(getHttpClientConnectionManager()));
        client.register(JacksonFeature.class);
        return client;
    }

    private ClientConfig getJerseyClientConfig(HttpClientConnectionManager clientConnectionManager) {
        return new ClientConfig(clientConnectionManager);
    }

    private HttpClientConnectionManager getHttpClientConnectionManager() {
        return new PoolingHttpClientConnectionManager();
    }

}
