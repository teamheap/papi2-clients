package uk.ac.cam.psychometrics.papi.client.jersey.model;

import org.codehaus.jackson.annotate.JsonValue;

public enum Trait {

    BIG5("BIG5"),
    BIG5_OPENNESS("BIG5_Openness"),
    BIG5_CONSCIENTIOUSNESS("BIG5_Conscientiousness"),
    BIG5_EXTRAVERSION("BIG5_Extraversion"),
    BIG5_AGREEABLENESS("BIG5_Agreeableness"),
    BIG5_NEUROTICISM("BIG5_Neuroticism"),

    SATISFACTION_WITH_LIFE("Satisfaction_Life"),
    INTELLIGENCE("Intelligence"),
    FEMALE("Female"),
    AGE("Age"),
    GAY("Gay"),
    LESBIAN("Lesbian"),

    CONCENTRATION("Concentration"),
    CONCENTRATION_ART("Concentration_Art"),
    CONCENTRATION_BIOLOGY("Concentration_Biology"),
    CONCENTRATION_BUSINESS("Concentration_Business"),
    CONCENTRATION_IT("Concentration_IT"),
    CONCENTRATION_EDUCATION("Concentration_Education"),
    CONCENTRATION_ENGINEERING("Concentration_Engineering"),
    CONCENTRATION_JOURNALISM("Concentration_Journalism"),
    CONCENTRATION_FINANCE("Concentration_Finance"),
    CONCENTRATION_HISTORY("Concentration_History"),
    CONCENTRATION_LAW("Concentration_Law"),
    CONCENTRATION_NURSING("Concentration_Nursing"),
    CONCENTRATION_PSYCHOLOGY("Concentration_Psychology"),

    POLITICS("Politics"),
    POLITICS_LIBERAL("Politics_Liberal"),
    POLITICS_CONSERVATIVE("Politics_Conservative"),
    POLITICS_UNINVOLVED("Politics_Uninvolved"),
    POLITICS_LIBERTANIAN("Politics_Libertanian"),

    RELIGION("Religion"),
    RELIGION_NONE("Religion_None"),
    RELIGION_CHRISTIAN_OTHER("Religion_Christian_Other"),
    RELIGION_CATHOLIC("Religion_Catholic"),
    RELIGION_JEWISH("Religion_Jewish"),
    RELIGION_LUTHERAN("Religion_Lutheran"),
    RELIGION_MORMON("Religion_Mormon"),

    RELATIONSHIP("Relationship"),
    RELATIONSHIP_NONE("Relationship_None"),
    RELATIONSHIP_YES("Relationship_Yes"),
    RELATIONSHIP_MARRIED("Relationship_Married");

    private final String name;

    Trait(String name) {
        this.name = name;
    }

    @JsonValue
    public String getName() {
        return name;
    }

}