package uk.ac.cam.psychometrics.papi.client.jersey.exception;

public class UsageLimitExceededException extends PapiClientException {

    public UsageLimitExceededException(String message) {
        super(message);
    }
}
