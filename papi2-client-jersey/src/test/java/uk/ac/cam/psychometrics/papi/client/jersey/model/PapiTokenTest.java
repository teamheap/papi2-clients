package uk.ac.cam.psychometrics.papi.client.jersey.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class PapiTokenTest {

    private static final String TOKEN_STRING_1 = "token";
    private static final String TOKEN_STRING_2 = "token2";
    private static final Long EXPIRES_1 = 1L;
    private static final Long EXPIRES_2 = 2L;

    @Test
    public void shouldEqualWhenHasSameTokenString() throws Exception {
        PapiToken token1 = new PapiToken(TOKEN_STRING_1, EXPIRES_1);
        PapiToken token2 = new PapiToken(TOKEN_STRING_1, EXPIRES_2);
        assertEquals(token1, token2);
        assertEquals(token1.hashCode(), token2.hashCode());
    }

    @Test
    public void shouldNotEqualWhenHasDifferentTokenString() throws Exception {
        PapiToken token1 = new PapiToken(TOKEN_STRING_1, EXPIRES_1);
        PapiToken token2 = new PapiToken(TOKEN_STRING_2, EXPIRES_1);
        assertNotEquals(token1, token2);
        assertNotEquals(token1.hashCode(), token2.hashCode());
    }

}
